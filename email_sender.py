"""
Copied from https://blog.macuyiko.com/post/2016/how-to-send-html-mails-with-oauth2-and-gmail-in-python.html
and adopted into a class.

Adapted from:
https://github.com/google/gmail-oauth2-tools/blob/master/python/oauth2.py
https://developers.google.com/identity/protocols/OAuth2

1. Generate and authorize an OAuth2 (generate_oauth2_token)
2. Generate a new access tokens using a refresh token(refresh_token)
3. Generate an OAuth2 string to use for login (access_token)
"""

import base64
import imaplib
import json
import smtplib
import urllib.parse
import urllib.request
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import lxml.html

class EmailSender:
    def command_to_url(self, command):
        return '%s/%s' % (self.GOOGLE_ACCOUNTS_BASE_URL, command)


    def url_escape(self, text):
        return urllib.parse.quote(text, safe='~-._')


    def url_unescape(self, text):
        return urllib.parse.unquote(text)


    def url_format_params(self, params):
        param_fragments = []
        for param in sorted(params.items(), key=lambda x: x[0]):
            param_fragments.append('%s=%s' % (param[0], self.url_escape(param[1])))
        return '&'.join(param_fragments)


    def generate_permission_url(self, client_id, scope='https://mail.google.com/'):
        params = {}
        params['client_id'] = client_id
        params['redirect_uri'] = self.REDIRECT_URI
        params['scope'] = scope
        params['response_type'] = 'code'
        return '%s?%s' % (self.command_to_url('o/oauth2/auth'), self.url_format_params(params))


    def call_authorize_tokens(self, client_id, client_secret, authorization_code):
        params = {}
        params['client_id'] = client_id
        params['client_secret'] = client_secret
        params['code'] = authorization_code
        params['redirect_uri'] = self.REDIRECT_URI
        params['grant_type'] = 'authorization_code'
        request_url = self.command_to_url('o/oauth2/token')
        response = urllib.request.urlopen(request_url, urllib.parse.urlencode(params).encode('UTF-8')).read().decode('UTF-8')
        return json.loads(response)


    def call_refresh_token(self, client_id, client_secret, refresh_token):
        params = {}
        params['client_id'] = client_id
        params['client_secret'] = client_secret
        params['refresh_token'] = refresh_token
        params['grant_type'] = 'refresh_token'
        request_url = self.command_to_url('o/oauth2/token')
        response = urllib.request.urlopen(request_url, urllib.parse.urlencode(params).encode('UTF-8')).read().decode('UTF-8')
        return json.loads(response)


    def generate_oauth2_string(self, username, access_token, as_base64=False):
        auth_string = 'user=%s\1auth=Bearer %s\1\1' % (username, access_token)
        if as_base64:
            auth_string = base64.b64encode(auth_string.encode('ascii')).decode('ascii')
        return auth_string


    def test_imap(self, user, auth_string):
        imap_conn = imaplib.IMAP4_SSL('imap.gmail.com')
        imap_conn.debug = 4
        imap_conn.authenticate('XOAUTH2', lambda x: auth_string)
        imap_conn.select('INBOX')


    def test_smpt(self, user, base64_auth_string):
        smtp_conn = smtplib.SMTP('smtp.gmail.com', 587)
        smtp_conn.set_debuglevel(True)
        smtp_conn.ehlo('test')
        smtp_conn.starttls()
        smtp_conn.docmd('AUTH', 'XOAUTH2 ' + base64_auth_string)


    def get_authorization(self, google_client_id, google_client_secret):
        scope = "https://mail.google.com/"
        print('Navigate to the following URL to auth:', self.generate_permission_url(google_client_id, scope))
        authorization_code = input('Enter verification code: ')
        response = self.call_authorize_tokens(google_client_id, google_client_secret, authorization_code)
        return response['refresh_token'], response['access_token'], response['expires_in']


    def refresh_authorization(self, google_client_id, google_client_secret, refresh_token):
        response = self.call_refresh_token(google_client_id, google_client_secret, refresh_token)
        return response['access_token'], response['expires_in']


    def send_mail(self, fromaddr, toaddr, subject, message):
        access_token, expires_in = self.refresh_authorization(self.GOOGLE_CLIENT_ID, self.GOOGLE_CLIENT_SECRET, self.GOOGLE_REFRESH_TOKEN)
        auth_string = self.generate_oauth2_string(fromaddr, access_token, as_base64=True)

        msg = MIMEMultipart('related')
        msg['Subject'] = subject
        msg['From'] = fromaddr
        msg['To'] = toaddr
        msg.preamble = 'This is a multi-part message in MIME format.'
        msg_alternative = MIMEMultipart('alternative')
        msg.attach(msg_alternative)
        part_text = MIMEText(lxml.html.fromstring(message).text_content().encode('utf-8'), 'plain', _charset='utf-8')
        part_html = MIMEText(message.encode('utf-8'), 'html', _charset='utf-8')
        msg_alternative.attach(part_text)
        msg_alternative.attach(part_html)
        server = smtplib.SMTP('smtp.gmail.com:587')
        server.ehlo(self.GOOGLE_CLIENT_ID)
        server.starttls()
        server.docmd('AUTH', 'XOAUTH2 ' + auth_string)
        server.sendmail(fromaddr, toaddr, msg.as_string())
        server.quit()



    def __init__(self, client_id, client_secret, fromAddr, refresh_token = None):
        self.GOOGLE_ACCOUNTS_BASE_URL = 'https://accounts.google.com'
        self.REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob'

        self.GOOGLE_CLIENT_ID = client_id
        self.GOOGLE_CLIENT_SECRET = client_secret
        self.GOOGLE_REFRESH_TOKEN = refresh_token

        self.fromAddr = fromAddr

        if self.GOOGLE_REFRESH_TOKEN is None:
            print('No refresh token found, obtaining one')
            refresh_token, access_token, expires_in = self.get_authorization(self.GOOGLE_CLIENT_ID, self.GOOGLE_CLIENT_SECRET)
            print('Set the following as your GOOGLE_REFRESH_TOKEN:', refresh_token)
            exit()

    def hasValidRefreshToken(self):
        if self.GOOGLE_REFRESH_TOKEN is not None:
            return True
        else:
            return False

    def sendEmail(self, toAddr, subject, body):
        if self.hasValidRefreshToken():
            self.send_mail(self.fromAddr, toAddr, subject, body)
        else:
            print("No valid refresh token. Obtain refresh token before sending. Did you remember to set it in the constructor?")
