import numpy as np
from random import randint

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-v', '--verbose',
                    action='store_true',
                    help='Verbose Output'
                    )

parser.add_argument('-t', '--test',
                    action='store_true',
                    help='Test run'
                    )
                    
args = parser.parse_args()

budget = 150 # sample value

# Read table into a numpy matrix
data = np.genfromtxt('people_real.tsv', delimiter=";", skip_header=1, dtype=str, filling_values=1)

# some simple checks on the format
if data.shape[0] == 0:
    print("Data doesn't have any rows.")
    quit()

if data.shape[1] != 3:
    print("Data doesn't have 3 columns.")
    quit()

print("Data found: ")

# Turn data into a dict where the key is the person's name. Each entry in the 
# dict is another dict which stores the email, name, and list of exclusions
people_list = {}
for person in data:
    
    name = person[0]
    email = person[1]
    exclusions = person[2]
    
    exclusions_set = set(exclusions.split(","))

    print(f"\t{name}: {email} -- Exluding: {exclusions_set}")
    people_list[name] = { 'name': name, 'email': email, 'exclusions': exclusions_set }

# Create a set of all possible pairs of matches (every person with every other person)
# with consideration to the exclusions. Each person's dict entry also gets a new key 'giftees'
# which is a list of all the people this person can give to
print("\nIterating all possible matches... ")
possible_pairs = []
for name0 in people_list:
    persons_giftees = []
    for name1 in people_list:
        if name0 == name1:
            continue
        if name1 in people_list[name0]['exclusions']: # Skip the 2nd person if they're in the 1st person's exclusions list
            continue
        pair = (name0, name1)
        possible_pairs.append(pair)
        persons_giftees.append(name1)
        if (args.verbose):
            print(f"\tFound potential pair: {pair}")
    
    people_list[name0]['giftees'] = persons_giftees


gift_graphs = [] # Holds all possible gifter -> giftee pair lists

#Recursive DFS through graph of participants
def walkgraph(people_graph, current_person, curr_path, all_paths):
    if len(curr_path) == len(people_graph) + 1:
        # print(f"Found chain: {curr_path}")
        all_paths.append(curr_path.copy())
        return

    for giftee in people_graph[current_person]['giftees']:
        # Rules for which routes are accepted.
        if giftee in curr_path and len(curr_path) != len(people_graph):
            continue
        if len(curr_path) > 2 and giftee == curr_path[-2]:
            continue
        if len(curr_path) == len(people_graph) and giftee != curr_path[0]:
            continue
        curr_path.append(giftee)
        walkgraph(people_graph, giftee, curr_path, all_paths)
        curr_path.pop()

    return

def startwalk(people_graph):
    all_paths = []
    for first_person in people_graph:
        curr_path = []
        curr_path.append(first_person)
        walkgraph(people_graph, first_person, curr_path, all_paths)

    return all_paths

print("\nWalking graph of all possible gift chains...\n")
final_paths = startwalk(people_list)

if len(final_paths) == 0:
    print("No valid gift chains found.")
    quit()

print("Found all all possible gift chains: ")

if args.verbose:
    i = 0
    for path in final_paths:
        print(f"{i}:\t{path}")
        i += 1

print("\nSelecting random gift chain...")
idx = randint(0, len(final_paths) - 1)
print(f"\t Selected chain {idx}.")

print(f"\nTurning gift chain {idx} into pairings...")

chain = final_paths[idx]
pairings = []

lastName = chain[0]
for i in range(1, len(chain)):
    pairings.append((lastName, chain[i]))
    lastName = chain[i]

for pair in pairings:
    if args.verbose:
        print(f"\t- {pair}")
    people_list[pair[0]]['assignee'] = pair[1]

from email_sender import EmailSender

email = np.genfromtxt('email_login.csv', delimiter=",", skip_header=1, dtype=str, filling_values=1)
if email.shape[0] == 3:
    client_id = email[1].strip()
    client_secret = email[2].strip()

    from_addr = email[0].strip()

    sender = EmailSender(client_id, client_secret, from_addr, None)
    quit()
elif email.shape[0] == 4:
    client_id = email[1].strip()
    client_secret = email[2].strip()

    from_addr = email[0].strip()

    refresh_token = email[3].strip()
    sender = EmailSender(client_id, client_secret, from_addr, refresh_token)
else:
    print("Invalid file for sender email. Should be a csv with a header, and 3 columns; one for email, one for client_id, and one for client_secret. Optionally, 4th column for refresh token")
    quit()


print("\nSending emails to participants:")
for person in people_list:
    addr = people_list[person]['email']
    assignee = people_list[person]['assignee']
    if addr and assignee:
        if not args.test:
            print(f"\tSending to {addr}:")
            bodyStr = f"""<h1> <p> Hello {person}! </p> </h1> 
                        <h2> 
                            <p> Your gift assignee for this year is <b>{assignee}</b>! </p>
                            <p> Just a reminder that the budget is <b>${budget}</b>. </p>
                            <p> Have fun! </p>
                        </h2> 
                        <img src="https://gifimage.net/wp-content/uploads/2017/10/dancing-santa-gif-8.gif" width=500px> </img>
                        """
        
            sender.sendEmail(addr, "Gift assignee", bodyStr)
            print("\tSent.")
        else:
            print(f"\tTest {addr}: didn't send.")